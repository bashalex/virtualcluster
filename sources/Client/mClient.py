#!/usr/bin/env python
import mLogger, sys, os
from mDaemon import VCDaemon
from mSettings import VCSettings as settings

if __name__ == "__main__":
    if not os.path.exists(settings.DAEMON_DIR):
            os.makedirs(settings.DAEMON_DIR)
    if not os.path.exists(settings.LOGS_DIR):
            os.makedirs(settings.LOGS_DIR)
    daemon = VCDaemon(settings.DAEMON_DIR + "daemon.pid")
    if len(sys.argv) > 1:
        if 'start' == sys.argv[1]:
            daemon.start()
        elif 'stop' == sys.argv[1]:
            daemon.stop()
        elif 'logs' == sys.argv[1]:
            mLogger.print_logs()
        elif 'clear_logs' == sys.argv[1]:
            mLogger.clear_logs()
        elif 'check' == sys.argv[1]:
            if len(sys.argv) < 3:
                print "Enter the name of process"
            else:
                print daemon.check_process(sys.argv[2])
        elif '--help' == sys.argv[1]:
            print 'It\'s a client for project VirtualCluster'
            print '  Usage:'
            print '\tstart - launch Daemon'
            print '\tstop - kill Daemon'
            print 'Before first launch, please configure the mSettings.py file'
        else:
            print "Unknown command. Type --help for more information"
    else:
        print "Unknown command. Type --help for information"
