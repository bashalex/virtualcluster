import time
import os
import mLogger
import httplib
import urllib
import subprocess
import json
import psutil
from datetime import datetime
from daemon import Daemon
from mSettings import VCSettings as settings


class VCDaemon(Daemon):
    cur_process = ""
    tick = 0
    process = None
    last_log_to_server = datetime.utcnow()
    task = []

    def run(self):
        while True:
            if self.process is None:
                print "send init request()"
                self.send_init_request()
            else:
                status = self.check_process(self.process)
                print "status: " + str(status)
                mLogger.log(status[1])
                if status[0] == 1:
                    self.relaunch_process()
                self.tick += 1
                if self.tick >= settings.REQUESTS_DELAY / settings.TICKS_DELAY:
                    self.send_info()
                    self.tick = 0
            time.sleep(settings.TICKS_DELAY)

    def check_process(self, name):
        if self.childProc is not None:
            proc = psutil.Process(self.childProc.pid)
            if proc.status() == psutil.STATUS_ZOMBIE:
                print "Find Zombie Process"
                self.childProc.kill()
                self.childProc = None
                return 1, "Process " + name + " isn't running, trying to relaunch it"
            else:
                return 0, "Process " + name + " with id " + str(self.childProc.pid) + " is running"
        else:
            return 1, "Process " + name + " isn't running, trying to relaunch it"

    def send_request(self, get=False, post=False, params=None):
        headers = {"Content-type": "application/x-www-form-urlencoded",
                   "Accept": "text/plain"}
        conn = httplib.HTTPConnection(settings.HOST, settings.PORT)
        if get and not post:
            conn.request("GET", "", params, headers)
        elif post and not get:
            conn.request("POST", "", params, headers)
        else:
            raise AttributeError
        return conn.getresponse()

    def send_info(self):
        self.last_log_to_server, new_logs = mLogger.get_new_logs(self.last_log_to_server)
        params = urllib.urlencode({'number': len(new_logs), "logs": new_logs, 'id': os.getpid()})
        self.send_request(post=True, params=params)

    def send_init_request(self):
        params = urllib.urlencode({'id': os.getpid()})
        response = self.send_request(get=True, params=params)
        if response.status == httplib.OK:
            answer = dict(json.loads(response.read()))
            print(answer)
            process = answer.get("process")
            if process is None:
                print("No task")
                mLogger.log("No task, sleep for a 10sec")
                time.sleep(10)
            else:
                self.process = process.get("name")
                params = dict(process.get("params"))
                self.task = [self.process]
                for key in params.keys():
                    self.task.append("--" + key)
                    self.task.append(params.get(key))
                print self.task
                self.relaunch_process()
                print "launch.."
                time.sleep(3)

    def relaunch_process(self):
        self.childProc = subprocess.Popen(self.task, stdin=None, stdout=None, stderr=None, close_fds=True)
        mLogger.log("Launch " + self.process + "...")
