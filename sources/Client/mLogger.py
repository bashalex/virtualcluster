import codecs
from mSettings import VCSettings as settings
from datetime import datetime


def log(text):
    with codecs.open(settings.LOG_FILE, 'a+', encoding='utf-8') as logs:
        time = str(datetime.utcnow())
        if not isinstance(text, unicode):
            text = unicode(text, 'utf-8', 'replace')
        message = "\n" + '#'*40 + "\n" + text + '\n' + time
        logs.write(message)


def error(text):
    with codecs.open(settings.ERROR_FILE, 'a+', encoding='utf-8') as errors:
        time = str(datetime.utcnow())
        if not isinstance(text, unicode):
            text = unicode(text, 'utf-8', 'replace')
        message = "\n" + '#'*40 + "\n" + text + '\n' + time
        errors.write(message)


def get_new_logs(last_log_to_server):
    new_logs = []
    with codecs.open(settings.LOG_FILE, 'r', encoding='utf-8') as logs:
        all_logs = logs.readlines()
        i = 0
        while i < len(all_logs) - 1:
            date = all_logs[len(all_logs) - i - 1]
            if datetime.strptime(date.rstrip(), "%Y-%m-%d %H:%M:%S.%f") >\
                    last_log_to_server:
                new_log = all_logs[len(all_logs) - i - 2]
                new_logs.append((date.rstrip(), new_log.rstrip()))
                i += 3
            else:
                break
        return datetime.utcnow(), new_logs


def print_logs():
    with codecs.open(settings.LOG_FILE, 'r', encoding='utf-8') as logs:
        for line in logs.readlines():
            print line.rstrip()


def clear_logs():
    codecs.open(settings.LOG_FILE, 'w', encoding='utf-8')
