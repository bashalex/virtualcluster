class VCSettings:
    DEBUG = False # Boolean
    DAEMON_DIR = "/var/run/virtualcluster/"
    LOGS_DIR = "/var/log/virtualcluster/"
    ERROR_FILE = LOGS_DIR + 'err.log'
    LOG_FILE = LOGS_DIR + 'log.log'
    HOST = "localhost"
    TICKS_DELAY = 2  # In seconds
    REQUESTS_DELAY = 6  # In seconds
    PORT = 1984
