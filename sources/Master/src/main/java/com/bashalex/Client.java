package com.bashalex;

/**
 * Created by bashalex on 08.11.15.
 */
public class Client {
    public String name;
    public int id;
    public int process;
    public String status = "OK";
    public long last_connection;

    public Client(String name, int id, int process, long last_connection) {
        this.name = name;
        this.id = id;
        this.process = process;
        this.status = "OK";
        this.last_connection = last_connection;
    }
}
