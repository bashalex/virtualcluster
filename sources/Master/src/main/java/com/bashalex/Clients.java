package com.bashalex;

import org.w3c.dom.Element;

/**
 * Created by bashalex on 08.11.15.
 */
public class Clients {
    private static Clients ourInstance = new Clients();
    private static String file = "clients.xml";

    public static Clients getInstance() {
        return ourInstance;
    }

    private Clients() {
    }


    public void addNew(Client node) {
        Element client;
        try {
            XMLDocument doc = new XMLDocument(file);

            // create new client
            client = doc.createElement("client");
            doc.setElementAttribute(client, "id", Integer.toString(node.id));

            doc.addElementParam(client, "name", node.name);
            doc.addElementParam(client, "process", Integer.toString(node.process));
            doc.addElementParam(client, "status", node.status);
            doc.addElementParam(client, "last_connection", Long.toString(node.last_connection));

            doc.addElement(client);

            // increment number
            int number = Integer.parseInt(doc.getAttribute("number"));
            doc.setElementAttribute("number", Integer.toString(++number));

            doc.commit();
            System.out.println("Client saved!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getNumber() {
        try {
            XMLDocument doc = new XMLDocument(file);

            return Integer.parseInt(doc.getAttribute("number"));
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public Client getClient(int i) {
        try {
            XMLDocument doc = new XMLDocument(file);

            int number = Integer.parseInt(doc.getAttribute("number"));
            if (i >= number) {
                return null;
            } else {
                String name = doc.getElementParam("client", i, "name");
                Integer process = Integer.parseInt(doc.getElementParam("client", i, "process"));
                long last_connection = Long.parseLong(doc.getElementParam("client", i, "last_connection"));
                int id = Integer.parseInt(doc.getElementAttribute("client", i, "id"));
                return new Client(name, id, process, last_connection);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void update(int id) {
        try {
            XMLDocument doc = new XMLDocument(file);
            int cur = 0;
            while (cur < doc.getElementsNumber("client")) {
                if (Integer.parseInt(doc.getElementAttribute("client", cur, "id")) == id) {
                    Element client = doc.getElement("client", cur);
                    doc.setElementParam(client, "last_connection", Long.toString(System.currentTimeMillis()));
                    break;
                } else {
                    ++cur;
                }
            }

            doc.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void removeClient(int id) {
        try {
            XMLDocument doc = new XMLDocument(file);

            int number = Integer.parseInt(doc.getAttribute("number"));

            int cur = 0;
            while (cur < doc.getElementsNumber("client")) {
                if (Integer.parseInt(doc.getElementAttribute("client", cur, "id")) == id) {
                    int process = Integer.parseInt(doc.getElementParam("client", cur, "process"));
                    doc.removeElement("client", cur);
                    --number;
                    Processes.getInstance().removeClient(process);
                    break;
                } else {
                    ++cur;
                }
            }
            doc.setElementAttribute("number", Integer.toString(number));

            doc.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clearAll() {
        try {
            XMLDocument doc = new XMLDocument(file);

            doc.setElementAttribute("number", "0");
            doc.clear();

            doc.commit();
            System.out.println("Clients list cleared!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
