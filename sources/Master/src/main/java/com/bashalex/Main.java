package com.bashalex;

/**
 * Created by bashalex on 21.10.15.
 */
public class Main {

    private static Thread t;
    private static Integer clientWaiting = 20; // in seconds
    private static Integer pause = 5; // in seconds

    public static void main(String[] args) throws Exception {
//
        // clear old session
        Clients.getInstance().clearAll();
        Processes.getInstance().clearAllClients();

//        // launch server
        new Master();

        t = new Thread(new CheckClientsStatus());
        t.start();
    }

    private static class CheckClientsStatus implements Runnable {
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    for (int i = 0; i < Clients.getInstance().getNumber(); ++i) {
                        Client client = Clients.getInstance().getClient(i);
                        if (System.currentTimeMillis() - client.last_connection > clientWaiting * 1000) {
                            System.out.print("Client with id: " + Integer.toString(client.id) +
                            " was lost. Remove it from list");
                            Clients.getInstance().removeClient(client.id);
                        }
                    }
                    Thread.sleep(pause * 1000);
                } catch (InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }
            }
        }
    }
}
