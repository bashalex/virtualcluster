package com.bashalex;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Created by bashalex on 21.10.15.
 */
class Master extends Thread {

    public static final int mPort = 1984;
    private HttpServer server;

    public Master () {
        try {
            server = HttpServer.create(new InetSocketAddress(mPort), 0);
            server.createContext("/", new MyHandler());
            server.createContext("/webpage", new WebPageHandler());
            server.setExecutor(null);
            server.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static class WebPageHandler implements HttpHandler {

        @Override
        public void handle(HttpExchange t) throws IOException {
            // request from Web Page
            Clients clients = Clients.getInstance();
            JSONObject response = new JSONObject();
            response.put("number", clients.getNumber());
            JSONArray clnts = new JSONArray();
            for (int i = 0; i < clients.getNumber(); ++i) {
                Client client = clients.getClient(i);
                Processes processes = Processes.getInstance();
                Process process = processes.getProcess(client.process);
                JSONObject clnt = new JSONObject();
                clnt.put("name", client.name);
                clnt.put("id", client.id);
                clnt.put("process id", process.id);
                clnt.put("process", process.name);
                clnt.put("last connection", client.last_connection);
                clnts.add(clnt);
            }
            response.put("clients", clnts);
            t.sendResponseHeaders(200, response.toJSONString().length());
            OutputStream os = t.getResponseBody();
            os.write(response.toJSONString().getBytes());
            os.close();
        }
    }

    static class MyHandler implements HttpHandler {

        @Override
        public void handle(HttpExchange t) throws IOException {
            if (t.getRequestMethod().equalsIgnoreCase("POST")) {
                int code = handlePostRequest(t);
                JSONObject response = new JSONObject();
                response.put("status", message(code));
                t.sendResponseHeaders(code, response.toJSONString().length());
                OutputStream os = t.getResponseBody();
                os.write(response.toJSONString().getBytes());
                os.close();
            } else if (t.getRequestMethod().equalsIgnoreCase("GET")) {
                JSONObject response = handleGetRequest(t);
                if (response == null) {
//                    return WebPage
                    File file = new File("web/index.html");
                    byte [] bytearray  = new byte [(int)file.length()];
                    FileInputStream fis = new FileInputStream(file);
                    BufferedInputStream bis = new BufferedInputStream(fis);
                    bis.read(bytearray, 0, bytearray.length);

                    t.sendResponseHeaders(200, file.length());
                    OutputStream os = t.getResponseBody();
                    os.write(bytearray,0,bytearray.length);
                    os.close();
                    return;
                } else {
                    t.sendResponseHeaders(200, response.toJSONString().length());
                }
                System.out.print(response.toJSONString() + "\n");
                OutputStream os = t.getResponseBody();
                os.write(response.toJSONString().getBytes());
                os.close();
            }
        }

        private int handlePostRequest(HttpExchange t) {
            try {
                Map<String, Object> parameters = new HashMap<>();
                InputStreamReader isr =
                        new InputStreamReader(t.getRequestBody(), "utf-8");
                BufferedReader br = new BufferedReader(isr);
                String query = br.readLine();
                parseQuery(query, parameters);
                System.out.print(parameters.get("number").toString() +
                        " new log(s)\n" + parameters.get("logs").toString() + "\n");
                Clients.getInstance().update(Integer.parseInt(parameters.get("id").toString()));
                return 200;
            } catch (Exception e) {
                return 400;
            }
        }

        private JSONObject handleGetRequest(HttpExchange t) {
            try {
                Map<String, Object> parameters = new HashMap<>();
                InputStreamReader isr =
                        new InputStreamReader(t.getRequestBody(), "utf-8");
                BufferedReader br = new BufferedReader(isr);
                String query = br.readLine();
                parseQuery(query, parameters);
                Clients clients = Clients.getInstance();
                // request from Client
                int num = clients.getNumber();
                String name = "Client " + num;
                Integer client_id = Integer.parseInt(parameters.get("id").toString());
                Process process = Processes.getInstance().getNextProcess();
                if (process != null) {
                    clients.addNew(new Client(name, client_id, process.id, System.currentTimeMillis()));
                    Processes.getInstance().setClient(process.id, client_id);
                    JSONObject response = new JSONObject();
                    JSONObject params = new JSONObject();
                    for (String elem : process.params.keySet()) {
                        params.put(elem, process.params.get(elem));
                    }
                    JSONObject proc = new JSONObject();
                    proc.put("name", process.name);
                    proc.put("params", params);
                    response.put("process", proc);
                    return response;
                } else {
                    JSONObject response = new JSONObject();
                    response.put("process", null);
                    return response;
                }
            } catch (Exception e) {
                return null;
            }
        }

        private String message(int code) {
            switch (code) {
                case 200:
                    return "OK";
                case 400:
                    return "Bad Request";
                default:
                    return "Bad Request";
            }
        }

        @SuppressWarnings("unchecked")
        private void parseQuery(String query, Map<String, Object> parameters)
                throws UnsupportedEncodingException {

            if (query != null) {
                String pairs[] = query.split("[&]");

                for (String pair : pairs) {
                    String param[] = pair.split("[=]");

                    String key = null;
                    String value = null;
                    if (param.length > 0) {
                        key = URLDecoder.decode(param[0],
                                System.getProperty("file.encoding"));
                    }

                    if (param.length > 1) {
                        value = URLDecoder.decode(param[1],
                                System.getProperty("file.encoding"));
                    }

                    if (parameters.containsKey(key)) {
                        Object obj = parameters.get(key);
                        if (obj instanceof List<?>) {
                            List<String> values = (List<String>) obj;
                            values.add(value);
                        } else if (obj instanceof String) {
                            List<String> values = new ArrayList();
                            values.add((String) obj);
                            values.add(value);
                            parameters.put(key, values);
                        }
                    } else {
                        parameters.put(key, value);
                    }
                }
            }
        }
    }
}