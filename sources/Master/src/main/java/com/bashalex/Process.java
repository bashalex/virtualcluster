package com.bashalex;

import java.util.HashMap;

/**
 * Created by bashalex on 08.11.15.
 */
public class Process {
    public String name;
    public int id;
    public HashMap<String, String> params = new HashMap<>();

    public Process(String name, int id, HashMap params) {
        this.name = name;
        this.id = id;
        this.params = params;
    }
}
