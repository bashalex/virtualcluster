package com.bashalex;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.HashMap;

/**
 * Created by bashalex on 08.11.15.
 */
public class Processes {
    private static Processes ourInstance = new Processes();
    private static String file = "processes.xml";

    public static Processes getInstance() {
        return ourInstance;
    }

    private Processes() {
    }

    public Process getProcess(int i) {
        try {
            XMLDocument doc = new XMLDocument(file);

            int number = Integer.parseInt(doc.getAttribute("number"));
            if (i >= number) {
                return null;
            } else {
                int id = Integer.parseInt(doc.getElementAttribute("process", i, "id"));
                String title = doc.getElementParam("process", i, "title");
                return new Process(title, id, null);
            }
        } catch (SAXException|IOException|ParserConfigurationException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Process getNextProcess() {
        try {
            XMLDocument doc = new XMLDocument(file);

            int number = Integer.parseInt(doc.getAttribute("number"));
            for (int i = 0; i < number; ++i) {
                Element process = doc.getElement("process", i);
                String client = doc.getElementParam("process", i, "client");

                if (client.equals("NULL")) {
                    int id = Integer.parseInt(doc.getElementAttribute("process", i, "id"));
                    String name = doc.getElementParam("process", i, "title");
                    HashMap<String, String> params = new HashMap<>();
                    NodeList process_params = process.getChildNodes();
                    for (int j = 0; j < process_params.getLength(); ++j) {
                        String param = process_params.item(j).getNodeName();
                        if (!param.equals("#text")) {
                            String value = process_params.item(j).getTextContent();
                            params.put(param, value);
                        }
                    }
                    return new Process(name, id, params);
                }
            }
            return null;
        } catch (SAXException|IOException|ParserConfigurationException e) {
            e.printStackTrace();
            return null;
        }
    }



    public void setClient(int process_id, int client_id) {
        try {
            XMLDocument doc = new XMLDocument(file);

            doc.setElementParam(doc.getElement("process", process_id),
                    "client", Integer.toString(client_id));
            doc.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void removeClient(int process_id) {
        try {
            XMLDocument doc = new XMLDocument(file);

            doc.setElementParam(doc.getElement("process", process_id),
                    "client", "NULL");
            doc.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clearAllClients() {
        try {
            XMLDocument doc = new XMLDocument(file);

            int number = Integer.parseInt(doc.getAttribute("number"));
            for (int i = 0; i < number; ++i) {
                doc.setElementParam(doc.getElement("process", i),
                        "client", "NULL");
            }
            doc.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
