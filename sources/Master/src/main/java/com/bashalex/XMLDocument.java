package com.bashalex;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by bashalex on 13.12.15.
 */
public class XMLDocument {

    private Document dom;
    private String file;
    private Element root;

    public XMLDocument(String file)
            throws ParserConfigurationException, SAXException, IOException {
        this.file = file;
        this.dom = DocumentBuilderFactory.newInstance()
                .newDocumentBuilder()
                .parse(getClass().getResourceAsStream(file));
        this.root = dom.getDocumentElement();
    }

    public void commit()
            throws FileNotFoundException, TransformerException {
        Transformer tr = TransformerFactory.newInstance().newTransformer();
        tr.setOutputProperty(OutputKeys.INDENT, "yes");
        tr.setOutputProperty(OutputKeys.METHOD, "xml");
        tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
        tr.transform(new DOMSource(dom),
                new StreamResult(new FileOutputStream(getClass().getResource(file).getFile())));

    }

    public String getAttribute(String attr) {
        return root.getAttribute(attr);
    }

    public Element getElement(Element parent, String name, int num) {
        return (Element) parent.getElementsByTagName(name).item(num);
    }

    public Element getElement(String name, int num) {
        return getElement(root, name, num);
    }

    public String getElementAttribute(String name, int num, String attr) {
        return getElement(name, num).getAttributes().getNamedItem(attr).getNodeValue();
    }

    public String getElementParam(String name, int num, String param) {
        return getElement(name, num).getElementsByTagName(param).item(0).getTextContent();
    }

    public void setElementParam(Element parent, String name, String value)
            throws FileNotFoundException, TransformerException {
        parent.getElementsByTagName(name).item(0).setTextContent(value);
    }

    public void setElementAttribute(Element element, String name, String value) {
        element.setAttribute(name, value);
    }

    public void setElementAttribute(String name, String value) {
       setElementAttribute(root, name, value);
    }

    public Element createElement(String name) {
        return dom.createElement(name);
    }

    public void addElementParam(Element element, String param, String value) {
        Element newParam = dom.createElement(param);
        newParam.setTextContent(value);
        element.appendChild(newParam);
    }

    public int getElementsNumber(String name) {
        return root.getElementsByTagName(name).getLength();
    }

    public void addElement(Element element) {
        root.appendChild(element);
    }

    public void removeElement(String name, int num) {
        root.removeChild(root.getElementsByTagName(name).item(num));
    }

    public void clearElement(Element element) {
        element.setTextContent("");
    }

    public void clear() {
        clearElement(root);
    }
}
